function IDCT = IDCTFirstPrinciples(blockStruct)
inputBlock = blockStruct.data(:,:); 
IDCT = zeros(size(inputBlock),'like',inputBlock); 

N = size(inputBlock,1); 
cosineIDCT = @(x,u,N) cos(((2*x+1)*u*pi)/(2*N));

for x = 1:N
    for y = 1:N
        
        temp = 0.0;
        
        for u = 1:N
            for v = 1:N
                alphaU = sqrt(1/N); 
                alphaV = sqrt(1/N);
                if u > 1
                    alphaU = sqrt(2/N); 
                end
                if v > 1
                    alphaV = sqrt(2/N);
                end
                % note that we address (u,v) this time since we're taking
                % elements from the transformed matrix. Rest remains the
                % same as g(x,y,u,v) = h(x,y,u,v)
                temp = temp + inputBlock(u,v)*alphaU*alphaV*cosineIDCT(x-1,u-1,N)*cosineIDCT(y-1,v-1,N); 
            end
        end
        
        IDCT(x,y) = temp; 
    end
end