image = imread('lena.bmp');
thresholdPercentage = 0.1; 

% if the image has three channels then it's RGB
if size(image,3) == 3
    image = im2double(rgb2gray(image)); 
% otherwise it's already grayscale
elseif size(image,3) == 1
    image = im2double(image); 
end

% can infer max. number of possible levels from image size
levels = log2(size(image,1));

% we constrain pyramid to have a maximum of 6 levels
if levels > 6
    levels = 6;
end

% declare cell array for gaussian pyramid
gaussianLevelsArray = cell(1,levels); 
% initial level is image itself
gaussianLevelsArray{1} = image; 

% laplacian pyramid will be 1 level shorter compared to gaussian
laplacianLevelsArray = cell(1,levels-1); 

% quantise using uencode and scale back between -1 to 1
quantise = @(inputValue,n) 2*double(uencode(inputValue,n))/(2^n-1) - 1; 

for i = 2 : levels
    gaussianLevelsArray{i} = REDUCE(gaussianLevelsArray{i-1}); % get next level of gaussian pyramid
    laplacianLevelsArray{i-1} = gaussianLevelsArray{i-1} - EXPAND(gaussianLevelsArray{i}); % next level of Lap. pyramid
end

bpp = 8; 
numberOfBits = 16*16*8; % this calculation is valid only when there are 6 levels

for i = levels - 1 : -1 : 1           
    % threshold laplacian matrix values
    maxMagnitude = max(abs(laplacianLevelsArray{i}(:))); 
    threshold = thresholdPercentage*maxMagnitude;
    laplacianLevelsArray{i}(abs(laplacianLevelsArray{i}) < threshold) = 0; 
    
    % number of bits for this level is number of non-zero co-efficients times
    % the bits per pixel for this level
    numberOfBits = numberOfBits + nnz(laplacianLevelsArray{i})*bpp;
    
    % go through all elements of current laplacian level
    for idx = 1:numel(laplacianLevelsArray{i})
        element = laplacianLevelsArray{i}(idx); 
        
        % only want to quantise if element is non-zero
        if element ~= 0
            laplacianLevelsArray{i}(idx) = quantise(element,bpp);
        end
    end
        
    bpp = bpp - 1;
end

disp(numberOfBits);

% to rebuild image, start off with the highest level gaussian
rebuiltImage = gaussianLevelsArray{levels}; 

for i = levels - 1 : -1 : 1
    rebuiltImage = EXPAND(rebuiltImage) + laplacianLevelsArray{i}; 
end

[peaksnr, snr] = psnr(rebuiltImage, image);

figure; 
subplot(1,2,1); imshow(image); 
subplot(1,2,2); imshow(rebuiltImage); 