function expandedMatrix = EXPAND(inputMatrix)
% use nearest neighbour interpolation for upscaling
upscaledMatrix = imresize(inputMatrix,2,'nearest'); 
% same kernel as used in REDUCE
gaussianKernel = fspecial('gauss',[5,5]);
expandedMatrix = conv2(upscaledMatrix, gaussianKernel, 'same'); 