%% CS413 Report - Image Compression using DCT and Pyramids
% This report will give an overview of the solutions to the different parts
% of the assignment and will also demonstrate their performance by application on
% test images.
%% Running instructions
% The DCT compression method is contained in the runnable script 
% 'questionDCT.m'. The input image and the threshold percentage to be applied 
% are specified at the top of the file and can be changed accordingly. At
% the bottom of the file are tests for the forward and inverse transforms,
% which can be run by uncommenting that section. 
%
% The Pyramid compression method is run using 'questionPyramid.m'. This
% file too specifies the input image and threshold at the top. 
%
% The remaining files are functions that are made use of in the above two
% methods.
%% DCT from first principles
% The Discrete Cosine Transform consists of the forward and 
% inverse transforms. The forward transform projects the original signal
% onto a set of cosine waves that act as basis functions, and is computed
% in the following manner.
%
% $$ T(u,v) = \sum\limits_{x = 0}^{N-1}\sum\limits_{y = 0}^{N-1} f(x,y)g(x,y,u,v) $$
%
% where $$ f(x,y) $$ is the image and $$ g(x,y,u,v) $$ are the 2D basis functions, 
% which are obtained by taking the product of the 1D basis functions $$ g(x,u) 
% $$ and $$ g(y,v) $$. The 1D kernel itself is defined as follows.
%
% $$ g(x,u) = \alpha (u) cos(\frac{(2x+1)u\pi}{2N}) $$
%
% with _x_ being the position and _u_ the frequency, and where
% $$ \alpha (u) $$ is $$ \sqrt{\frac{1}{N}} $$ for _u_ = 0 and $$ \sqrt{\frac{2}{N}} $$
% otherwise.
%
% This is implemented using a nested for-loop, the outer component
% of which goes over all the _u's_ and _v's_, and the inner which goes over
% all the _x's_ and _y's_. The transform output has the same size as the input, and each _(u,v)_ 
% represents the frequency co-ordinates of this output. Frequency
% co-ordinates are calculated in turn in iterations of the outer
% loop (not shown here).
for x = 1:N
    for y = 1:N
        temp = temp + inputBlock(x,y)*alphaU*alphaV*cosineDCT(x-1,u-1,N)*cosineDCT(y-1,v-1,N); 
    end
end
%%
% _cosineDCT_ here is an anonymous function which computes the cosine
% components of the 1D kernels.
%
cosineDCT = @(x,u,N) cos(((2*x+1)*u*pi)/(2*N)); 
%%
% The inverse transform is very similar to the forward one.
%
% $$ f(x,y) = \sum\limits_{u = 0}^{N-1}\sum\limits_{v = 0}^{N-1} T(u,v)g(x,y,u,v) $$
%
% The kernel here is the same as in the
% forward transform. The implementation too is similar, the only major
% difference being that the _x's_ and _y's_ are now part of the outer loop
% while the _u's_ and _v's_ form the inner one. 
%
% The implementations of the forward and inverse transforms can be found in
% 'DCTFirstPrinciples.m' and 'IDCTFirstPrinciples.m' respectively. 
%% Block processing of DCT
% The Discrete Cosine Transform is typically applied on distinct blocks of
% the input image to create a set of local
% spectra. The image is first partitioned into blocks of
% equal sizes (8 x 8 or some other power of 2) and the transform is applied 
% separately on each block. 
%
% In the implementation this is handled by using Matlab's in-built
% _blockproc_ function that takes as input the matrix on which the block
% processing needs to be applied, the size of each block and the function
% that should be applied on each block. For example, it is used as follows
% for the forward transform.
dct = blockproc(image,[8 8],@DCTFirstPrinciples);
%%
% Once the input function has been applied on all the generated blocks,
% _blockproc_ returns a matrix with all modified
% blocks in their original positions. Thus 
% the transform can easily work for blocks of arbitrary size such as 16 x 16 or 64 x 64 by simply
% specifying the same in the input to _blockproc_. 
%% Testing DCT for correctness
% The correctness of the implemented forward and inverse transforms can be
% tested by making use of the in-built Matlab functions _dct2_ and _idct2_
% that do the same. 
fun = @(block_struct) dct2(block_struct.data);
D = blockproc(image,[8 8],fun);
%%
% Outputs from the corresponding methods are then compared to see whether
% the implemented method produces the same result as the in-built function.
% By nature of floating point operations two double
% values will never be exactly the same, so some small, acceptable tolerance 
% must be defined within which these values can be considered to be equal. 
tol = 0.0001; 
disp(nnz(abs(dct-D) > tol) == 0);
%% DCT Image Compression 
% The DCT transform by itself does not lead to any reduction in the size of
% the image. Compression is achieved by discarding co-efficients from the
% transformed matrix that have small or zero magnitudes. These are usually
% considerable in number and thus lead to a significant reduction in storage 
% space required. 
%
% A common way of selecting which co-efficients to discard is by
% thresholding. Co-efficients above this prescribed threshold are retained
% while the ones below are discarded. The threshold itself is determined by
% taking some small proportion of the maximum absolute magnitude observed.
%find the maximum absolute value in the matrix
maxMagnitude = max(abs(compressedMatrix(:)));
%take threshold to be some percentage of this max value
threshold = thresholdPercentage*maxMagnitude; 
%%
% Once the threshold is obtained, co-efficients that have magnitudes below
% it are discarded by having their mangitudes set to 0. 
% The remaining non-zero co-efficients are then quantised using 8 bits. 
% Assuming that co-efficients set to 0 do not require any bits to be
% represented (that is, we assume no overhead of run length or any other
% encoding scheme), the total number of bits required can be calculated as follows. 
numberOfBits = nnz(compressedMatrix)*8; 
%%
% The performance of this compression algorithm is demonstrated by the
% following rate-distortion curve obtained by applying it on three images.
% Two of these are the standard Barbara and Lena images, while the third
% is one that I have chosen - 'Kongou.png'. The values are obtained by
% applying thresholds of 10%, 5%, 3% and 1% respectively. 
% 
% <<DCT Compression Performance.png>>
%%
% Thus it can be seen that the compression on Lena far outperforms that on
% Barbara and Kongou. 
% 
% <<kongouDCT.png>>
% 
%% Gaussian and Laplacian Pyramid
% Another approach to compression is utilising image pyramids.
% Such pyramids are constructed by subsampling and smoothing using Gaussian
% kernels. The two fundamental operations that underpin this method are 
% REDUCE and EXPAND. 
% 
% The REDUCE operation filters the input image using a 2D Gaussian kernel and
% then subsamples it. The result of REDUCE becomes a new level of the
% pyramid. Recursive application of this operation thus leads to a set of
% pyramid levels. REDUCE is implemented using the in-built 
% functions _fspecial_ for creating Gaussian filters and _conv2_ for carrying
% out 2D convolution.
%5x5 kernel as used in Burt and Adelson paper
gaussianKernel = fspecial('gauss',[5,5]);
%apply convolution using kernel declared above
smoothedMatrix = conv2(inputMatrix, gaussianKernel, 'same');
%subsample by taking every other pixel
reducedMatrix = smoothedMatrix(1:2:end,1:2:end); 
%%
% The EXPAND operation is used to go down the pyramid, and works by
% up-sampling by a factor of 2 and then smoothing again in order to 
% interpolate. Up-sampling is carried out using the _imresize_ function 
% and _conv2_ again.
%use nearest neighbour interpolation for upscaling
upscaledMatrix = imresize(inputMatrix,2,'nearest'); 
%same kernel as used in REDUCE
gaussianKernel = fspecial('gauss',[5,5]);
expandedMatrix = conv2(upscaledMatrix, gaussianKernel, 'same'); 
%%
% Using these REDUCE and EXPAND operations leads to the creation of a
% Gaussian pyramid. A Laplacian pyramid is a similar concept, with the
% pyramid created by applying a Laplacian operator to each level of a
% Gaussian pyramid. It turns out that a Difference of Gaussians (DoG) is
% a good approximation of the Laplacian, and is easier to compute. Thus
% levels of a Laplacian pyramid are created by expanding the Gaussian level
% above (using EXPAND) and subtracting it from the current Gaussian level.
%
% Gaussian and Laplacian pyramids are implemented by making use of cell
% arrays. Each level of the pyramid is stored in a separate array index.
% Assuming images of size $$ 2^N $$, the number of levels in the pyramid is
% obtained by taking _log2(imageSize)_.
%declare cell array for gaussian pyramid
gaussianLevelsArray = cell(1,levels); 
%initial level is image itself
gaussianLevelsArray{1} = image; 
%laplacian pyramid will be 1 level shorter compared to gaussian
laplacianLevelsArray = cell(1,levels-1); 
%%
% Note that the Laplacian array is one level shorter as computing any
% Laplacian level requires the presence of the next Gaussian level. The
% pyramids are then filled using the REDUCE and EXPAND operations 
for i = 2 : levels
    gaussianLevelsArray{i} = REDUCE(gaussianLevelsArray{i-1}); %get next level of gaussian pyramid
    laplacianLevelsArray{i-1} = gaussianLevelsArray{i-1} - EXPAND(gaussianLevelsArray{i}); %next level of Lap. pyramid
end
%% Laplacian pyramid compression
% Compression is achieved by carrying out thresholding and
% quantisation of Laplacian co-efficients. 
% Burt and Adelson, the authors of
% the original paper on the DoG method of compression, noted that humans
% are less sensitive to quantisation errors at high spatial frequencies.
% Thus while low-frequency co-efficients, corresponding to higher pyramid
% levels, require more bits for representation, high-frequency co-efficients
% corresponding to lower pyramid levels can be encoded using fewer bits
% without resulting in significant image quality loss. Thus the
% implementation of the Laplacian pyramid method quantises co-efficients at 
% different levels at different numbers of bits, with higher levels
% allocated more bits. 
bpp = 8; 
for i = levels - 1 : -1 : 1      
    %...
    %thresholding takes place before
    
    %go through all elements of current laplacian level
    for idx = 1:numel(laplacianLevelsArray{i})
        element = laplacianLevelsArray{i}(idx); 
        
        %only want to quantise if element is non-zero
        if element ~= 0
            laplacianLevelsArray{i}(idx) = quantise(element,bpp);
        end
    end
    
    bpp = bpp - 1;
end
%%
% The quantisation is carred out by an anonymous function, which makes use
% of the _uencode_ method.
quantise = @(inputValue,n) 2*double(uencode(inputValue,n))/(2^n-1) - 1;
%% 
% _uencode_ returns integers in the range 0 to $$ 2^N $$ - 1, but we require
% the returned values to be in the original range of -1 to 1 and hence the 
% appropriate scaling is applied. 
% 
% Once the thresholding and quantisation are applied, only the Laplacian
% pyramid and the final level of the Gaussian pyramid are required to
% produce the compressed form of the original image. 
rebuiltImage = gaussianLevelsArray{levels}; 

for i = levels - 1 : -1 : 1
    rebuiltImage = EXPAND(rebuiltImage) + laplacianLevelsArray{i}; 
end
%% Testing pyramids for correctness
% The created pyramids can be tested for correctness by removing the
% thresholding and quantisation, and checking whether the reconstructed
% image using the final Gaussian level and the whole Laplacian pyramid is
% exactly the same as the original image. This testing process was followed
% with the images being compared using the tolerance method described
% earlier and they were found to be equal, hence proving correctness. 
%% Comparing DCT and Pyramid compression
% The two compression methods are first compared by applying them on the Lena
% image. The DCT values on the rate-distortion curve are obtained by again
% applying thresholds of 1%, 3%, 5% and 10% as before. The pyramid values
% are obtained by using a 15% threshold at each Laplacian level and also
% applying four variable quantisation schemes from levels 5-1 : 8/7/6/5/4 
% bits, 7/6/5/4/3 bits, 6/5/4/3/2 bits and 5/4/3/2/2 bits.
%%
% 
% <<DCT vs Pyramid Compression Performance.PNG>>
% 
% Thus it can be seen that the DCT approach produces better quality compression
% throughout the entire range of bits per pixel considered. The procedure
% is repeated with the same parameters on the Kongou image.
%%
% 
% <<DCT vs Pyramid Compression Performance - Kongou.PNG>>
% 
% Performance is a bit closer in this case, but the DCT approach again
% proves superior. Thus we can conclude that the DCT method produces 
% better quality compression than the Pyramid scheme. 
%
% Word count - 1499 (excluding titles and code excerpts)
%

