function compressedMatrix = compressDCT(dct,thresholdPercentage)
compressedMatrix = dct; 

% find the maximum absolute value in the matrix
maxMagnitude = max(abs(compressedMatrix(:))); 
% take threshold to be some percentage of this max value
threshold = thresholdPercentage*maxMagnitude; 

% set all values that have absolute value below threshold to be 0
compressedMatrix(abs(compressedMatrix) < threshold) = 0; 