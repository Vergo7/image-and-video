image = im2double(rgb2gray(imread('kongou.png')));
kongou3Percent = im2double(imread('kongou3Percent.png'));
kongou5Percent = im2double(imread('kongou5Percent.png'));
kongou10Percent = im2double(imread('kongou10Percent.png'));

figure;
subplot(2,2,1); imshow(image); title('Original Kongou');
subplot(2,2,2); imshow(kongou3Percent); title('3% threshold');
subplot(2,2,3); imshow(kongou5Percent); title('5% threshold');
subplot(2,2,4); imshow(kongou10Percent); title('10% threshold');

