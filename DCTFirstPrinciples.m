function DCT = DCTFirstPrinciples(blockStruct)
% get the data from the blockStruct and store it in a matrix
inputBlock = blockStruct.data(:,:); 
% create output DCT matrix to be same size as input matrix
DCT = zeros(size(inputBlock),'like',inputBlock); 

% square matrix, so number of rows suffices for N
N = size(inputBlock,1); 
% anonymous function to calculate cosine parts during DCT calculation
cosineDCT = @(x,u,N) cos(((2*x+1)*u*pi)/(2*N)); 

for u = 1:N
    for v = 1:N
        alphaU = sqrt(1/N); 
        alphaV = sqrt(1/N);
        if u > 1
            alphaU = sqrt(2/N); 
        end
        if v > 1
            alphaV = sqrt(2/N);
        end
        
        temp = 0.0; 
        
        for x = 1:N
            for y = 1:N
                % minus 1's because the formula has x,y,u and v going from
                % 0 to N-1 while the indicies in matlab begin from 1.
                % inputblock has x and y unchanged because they address
                % elements in the matrix, and need to correspond to
                % 1-indexed
                temp = temp + inputBlock(x,y)*alphaU*alphaV*cosineDCT(x-1,u-1,N)*cosineDCT(y-1,v-1,N); 
            end
        end
                
        DCT(u,v) = temp; 
    end
end
