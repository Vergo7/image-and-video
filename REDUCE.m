function reducedMatrix = REDUCE(inputMatrix)
% 5x5 kernel as used in Burt and Adelson paper
gaussianKernel = fspecial('gauss',[5,5]);
% apply convolution using kernel declared above
smoothedMatrix = conv2(inputMatrix, gaussianKernel, 'same');
% subsample by taking every other pixel
reducedMatrix = smoothedMatrix(1:2:end,1:2:end); 