image = imread('lena.bmp');
threshold = 0.1; % specify threshold used in DCT compression, e.g, 10% = 0.1

% quantise using 8 bits, and then scale values back to the original range
quantise = @(inputValue,maxValue) maxValue*(2*double(uencode(inputValue,8,maxValue))/(2^8-1)-1); 

% if the image has three channels then it's RGB
if size(image,3) == 3
    image = im2double(rgb2gray(image)); 
% otherwise it's already grayscale
elseif size(image,3) == 1
    image = im2double(image); 
end

% apply DCT separately on distinct 8x8 blocks of image
dct = blockproc(image,[8 8],@DCTFirstPrinciples);
compressedMatrix = compressDCT(dct,threshold); 

% input range for uencode should be -maxAbsValue to +maxAbsValue
quantiseRange = ceil(max(abs(compressedMatrix(:))));
% go through each value in the compressedMatrix
for idx = 1:numel(compressedMatrix)
    element = compressedMatrix(idx); 
    
    % we only want to quantise the non-zero elements
    if element ~= 0       
        compressedMatrix(idx) = quantise(element,quantiseRange); 
    end
end

idct = blockproc(compressedMatrix,[8 8],@IDCTFirstPrinciples);

figure; 
subplot(1,2,1); imshow(image); 
subplot(1,2,2); imshow(idct);

% assume no encoding overhead, so 0 value co-efficients need no bits
numberOfBits = nnz(compressedMatrix)*8; 
[peaksnr, snr] = psnr(idct, image);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % Tests for implemented functions
% 
% % to check whether the transform and inverse transform are correct, use the
% % in-built functions 'dct2' and 'idct2'
% fun = @(block_struct) dct2(block_struct.data);
% D = blockproc(image,[8 8],fun);
% 
% % http://stackoverflow.com/questions/2202641/how-do-i-compare-all-elements-of-two-arrays
% tol = 0.0001; 
% % if any elements b/w the two matrices are not equal, return 1
% % if resulting logical matrix has no NNZ elements then matrices are equal
% disp(nnz(abs(dct-D) > tol) == 0); 
% 
% ifun = @(block_struct) idct2(block_struct.data);
% I = blockproc(D,[8 8],ifun); 
% % note that we take dct as argument here instead of compressedMatrix
% idctTest = blockproc(dct,[8 8],@IDCTFirstPrinciples);
% 
% disp(nnz(abs(idctTest-I) > tol) == 0); 